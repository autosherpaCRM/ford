/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package repositories;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;

import configs.JinqSource;
import controllers.webmodels.AssignedListForCRE;
import controllers.webmodels.CustomerDataOnTabLoad;
import controllers.webmodels.PSFFollowupNotificationModel;

import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;

import play.Logger.ALogger;
import play.db.DB;

import javax.persistence.*;
import javax.sql.DataSource;
import models.SMSParameters;
import models.ServiceTypes;
import models.CallInteraction;
import models.ServiceBooked;
import models.Customer;
import models.SRDisposition;
import models.PickupDrop;
import models.Vehicle;
import models.ServiceAdvisor;
import models.Workshop;
import models.Driver;
import models.UploadData;
import models.SMSTemplate;
import models.SMSstatus;
import models.Role;
import models.SMSInteraction;
import models.SavedSearchResult;
import com.google.firebase.database.FirebaseDatabase;
import com.mysql.fabric.xmlrpc.base.Array;
import com.fasterxml.jackson.databind.JsonNode;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseReference;

import java.net.URI;
import java.net.URISyntaxException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.logging.Level;
import java.util.logging.Logger;
import models.Address;

import models.AssignedInteraction;
import models.CallDispositionData;
import models.Campaign;
import models.JobCardDetailsUpload;
import models.NewCustomer;
import models.PSFAssignedInteraction;
import models.Service;
import models.UploadFileFields;
import models.UploadMasterFormat;
import models.WyzUser;
import models.workshopBill;
import models.Complaint;
import models.CustomerJson;
import models.Email;
import models.InsuranceDisposition;
import models.Location;
import models.MessageParameters;
import models.Phone;
import models.Segment;
import models.TaggingUsers;
import models.Tenant;
import models.UnAvailability;
import models.Workshop;
import models.WorkshopSummary;
import models.ComplaintInteraction;
import models.ComplaintAssignedInteraction;
import models.CustomerSearchResult;

import org.apache.http.client.utils.URIBuilder;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Session;

/**
 *
 * @author W-1004
 */
@Repository("SMSTemplateRepository")
@Transactional
public class SMSTemplateRepositoryImpl implements SMSTemplateRepository {

	ALogger logger = play.Logger.of("application");

	@PersistenceContext
	private EntityManager em;

	@Autowired
	private JinqSource source;
	
	@Autowired
	private AllCallInteractionRepository all_call_int_repo;


	
	@Override
	public List<CustomerSearchResult> getCustomerList(long savedsearchname) {
		logger.info("saved search name repo:"+savedsearchname);
		long count = source.savedSearchs(em).where(searchresult -> searchresult.getId()==savedsearchname)
				.count();
		if (count > 0) {
			SavedSearchResult result = source.savedSearchs(em)
					.where(searchresult -> searchresult.getId() == savedsearchname).getOnlyValue();
			Hibernate.initialize(result.getSearchResults());
			return result.getSearchResults();
		}
		return null;
	}

	@Override
	public List<MessageParameters> getAllMsgParameters() {
		return source.messageParameters(em).toList();

	}

	@Override
	public List<SMSTemplate> getAllSMSTemplate() {
		return source.smsTemplates(em).toList();

	}

	@Override
	public List<SavedSearchResult> getAllSavedSearchList() {
		return source.savedSearchs(em).toList();

	}


	@Override
	
	public SMSTemplate addNewSMSTemplate(String msgTemp,String msgapi,String userLoginName ,String userLogindealerCode){
		logger.info("inside sms template:"+msgTemp);
		SMSTemplate newSMSTemplate = new SMSTemplate();
		newSMSTemplate.setSmsTemplate(msgTemp);
		newSMSTemplate.setSmsAPI(msgapi);
		newSMSTemplate.setDealer(userLogindealerCode);
		newSMSTemplate.setDealerName(userLogindealerCode);
		newSMSTemplate.setSmsType("New Template");

		em.persist(newSMSTemplate);
	
		long smsId=source.smsTemplates(em).max(u -> u.getSmsId());
		logger.info("inside sms smsId:"+smsId);

		SMSTemplate latestSMSSaved = source.smsTemplates(em).where(u -> u.getSmsId()==smsId).getOnlyValue();
		return latestSMSSaved;
	}
	
	/*@Override
	public SMSTemplate addNewSMSTemplate(String messageTemplate,String userLoginName ,String userLogindealerCode){
		logger.info("inside sms template:"+messageTemplate);
		SMSTemplate newSMSTemplate = new SMSTemplate();
		newSMSTemplate.setSmsTemplate(messageTemplate);
		newSMSTemplate.setSmsAPI(msgAPI);
		em.persist(newSMSTemplate);
		
		long smsId=source.smsTemplates(em).max(u -> u.getSmsId());
		SMSTemplate latestSMSSaved = source.smsTemplates(em).where(u -> u.getSmsId()==smsId).getOnlyValue();
		return latestSMSSaved;
	}*/
	
	@Override 
	public List<CustomerSearchResult> getFilteredSearchResult(long savedName, List<Long> ids, boolean selectAllFlag){
		SavedSearchResult savedresult = source.savedSearchs(em).where(u->u.getId()==savedName).getOnlyValue();
		
		logger.info("repo savedresult : "+savedresult.getSavedName()+ " selectAllFlag : "+selectAllFlag);
		
		Hibernate.initialize(savedresult.getSearchResults());
		if(selectAllFlag == true){
			return savedresult.getSearchResults();
		}else{
			List<CustomerSearchResult> customers 		 = savedresult.getSearchResults();
			List<CustomerSearchResult> filteredCustomers = customers.stream().filter(customer-> ids.contains(customer.getId())).collect(Collectors.toList());
			return filteredCustomers; 
			
		}
		
	}

	@Override
	public Map<String, String> getMessagesForSMSBlast(String userLoginName,long savedName, List<Long> searchResultIds, long smstemplateId,
			boolean selectAll) {
		// TODO Auto-generated method stub
		
		logger.info("inside getMessageForSMSBlast:"+smstemplateId);
		logger.info("inside getmsgblaast selectall value:"+selectAll);
		logger.info("inside getmsgblaast saved name long:"+savedName);
		SMSTemplate smsTemplateData = source.smsTemplates(em).where(u -> u.getSmsId() == smstemplateId).getOnlyValue();
		String smsUrl = smsTemplateData.getSmsAPI();
		
		
		//com.typesafe.config.Config configuration = com.typesafe.config.ConfigFactory.load();
		//String smsUrl 							 = configuration.getString("app.smsUrl");
		
		URIBuilder uribuilder;

		List<Map<String, String>> smsbulkList 	= new ArrayList<Map<String, String>>();
		SavedSearchResult savedResults 			= source.savedSearchs(em).where(u->u.getId()== savedName).getOnlyValue();
		SMSTemplate smsData 					= source.smsTemplates(em).where(u->u.getSmsId() == smstemplateId).getOnlyValue();
		WyzUser		userinfo 				    = source.wyzUsers(em).where(u->u.getUserName() == userLoginName).getOnlyValue();

		Hibernate.initialize(savedResults.getSearchResults());
		
		
		Map<String,String> dataList 			= new HashMap<String,String>();

		 List<CustomerSearchResult> custList 	= getFilteredSearchResult(savedName,searchResultIds,selectAll);
		 
		 logger.info("search results id new:"+ searchResultIds);
		 String nextServiceDate;
		for (CustomerSearchResult custdata : custList) {
			
			String phonenum 		= custdata.getPhoneNumber();
			String smsMsgTemplate 	= smsData.getSmsTemplate();
			String custName 		= custdata.getCustomerName();
			String model 			= custdata.getModel();
			String vehRegNum 		= custdata.getVehicleRegNo();
			String userphone 		= userinfo.getPhoneNumber();

			
			if(custdata.getNextServicedate() != null){
			 nextServiceDate 	= custdata.getNextServicedate();				
			}else{
				nextServiceDate 	= "";
			}
			String variant 			= custdata.getVariant();
			String Dealer 			= smsData.getDealer();
			String messageTemplate 	= smsMsgTemplate;
			messageTemplate			= messageTemplate.replace("(customer name)", custName);
			messageTemplate			= messageTemplate.replace("(Model)", model);
			messageTemplate			= messageTemplate.replace("(Vehicle)",vehRegNum);
			messageTemplate			= messageTemplate.replace("(ServiceDueDate)", nextServiceDate);
			messageTemplate			= messageTemplate.replace("(PhoneNumber)", phonenum);
			messageTemplate			= messageTemplate.replace("(Variant)", variant);
			messageTemplate			= messageTemplate.replace("(Dealer)", Dealer);
			messageTemplate			= messageTemplate.replace("(phonenumber)",userphone);

			logger.info("messageTEmplate/:"+messageTemplate);
			logger.info("nextservicedate : "+nextServiceDate);
			logger.info("dealer name is:"+Dealer);
			dataList.put(phonenum, messageTemplate);
			smsbulkList.add(dataList);
		}
		SendBulkSMSToUsers(userLoginName,dataList,smstemplateId);

		return dataList;
	}




public boolean SendBulkSMSToUsers(String userLoginName,Map<String, String> dataList, long smstemplateId){
		
		//	com.typesafe.config.Config configuration = com.typesafe.config.ConfigFactory.load();
			//String smsUrl 							 = configuration.getString("app.smsUrl");		
			URIBuilder uribuilder;
			
			SMSTemplate smsTemplateData = source.smsTemplates(em).where(u -> u.getSmsId() == smstemplateId).getOnlyValue();
			String smsUrl = smsTemplateData.getSmsAPI();
			String dealerName = smsTemplateData.getDealerName();
			String Dealer = smsTemplateData.getDealer();
			
			logger.info("sms api:"+smsUrl);
		
			for (Map.Entry<String, String> entry : dataList.entrySet()) {
				System.out.println("Key for error is = " + entry.getKey() + ", Value = " + entry.getValue());
				String phonenum = entry.getKey();
				String messageTemplate = entry.getValue();
				//String dealerName = entry.


				WyzUser wyzUserName = source.wyzUsers(em).where(u-> u.getUserName().equals(userLoginName)).getOnlyValue();
				Customer customeris=new Customer();
				Vehicle vehId = new Vehicle();
				
				long customerCount=source.phones(em).where(u -> u.getPhoneNumber().equals(phonenum)).select(u-> u.getCustomer()).count();
				
				//logger.info(" customerCount for phone number : "+customerCount);
				// customer id is in the vehicle
				if(customerCount>0){
				customeris=source.phones(em).where(u -> u.getPhoneNumber().equals(phonenum)).select(u-> u.getCustomer()).toList().get(0);
				long custId = customeris.getId();
				vehId = source.vehicle(em).where(u -> u.getCustomer().getId() == custId).toList().get(0);
				}
				
				try {
				
				
				uribuilder = new URIBuilder(smsUrl);
				/*uribuilder.addParameter("mobile", phonenum);
				uribuilder.addParameter("source", "ATULMS");
				uribuilder.addParameter("message", messageTemplate);
				uribuilder.addParameter("senderid", "ATULMS");
				uribuilder.addParameter("accusage", "1");*/
				List<SMSParameters> smsParameters = source.smsParameters(em).toList();
				List<SMSstatus> statuscodes=all_call_int_repo.getAllSMSstatuscode();
				
				SMSParameters smsSentParam = smsParameters.get(0);
				String sentSuccess = smsSentParam.getSucessStatus();
				logger.info("sentSuccess:"+sentSuccess);

				for(SMSParameters listParams:smsParameters){
					String phone = listParams.getPhone();
					String message = listParams.getMessage();
					String senderid= listParams.getSenderid();				
					logger.info("phone , message , senderid:"+phone);
					logger.info("phone , message , senderid:"+message);
					logger.info("phone , message , senderid:"+senderid);
					uribuilder.addParameter(phone, phonenum);
					uribuilder.addParameter(message, messageTemplate);
					uribuilder.addParameter(senderid, dealerName);
				}
				URI uri = uribuilder.build();

				RestTemplate restTemplate = new RestTemplate();

				ResponseEntity<String> response = restTemplate.getForEntity(uri, String.class);

				if (response.getStatusCode() == HttpStatus.OK) {
					String responseBody = response.getBody();
					logger.info("Response Body:" + responseBody);

					// Insert into SMS interaction table
					String pattern = "dd/MM/yyyy";
					String pattern1 = "HH:mm:ss";

					// logger.info("time_zone :"+time_zone);
					java.util.TimeZone tz = java.util.TimeZone.getTimeZone("IST");
					Calendar c 			  = Calendar.getInstance(tz);
					Date now 			  = c.getTime();

					SimpleDateFormat simpleDateFormat  = new SimpleDateFormat(pattern);
					SimpleDateFormat simpleDateFormat1 = new SimpleDateFormat(pattern1);

					simpleDateFormat.setTimeZone(tz);
					simpleDateFormat1.setTimeZone(tz);

					String date = simpleDateFormat.format(now);
					String time = simpleDateFormat1.format(now);

					SMSInteraction smsInteraction = new SMSInteraction();
					smsInteraction.setCustomer(customeris);
					smsInteraction.setVehicle(vehId);
					smsInteraction.setMobileNumber(phonenum);
					smsInteraction.setWyzUser(wyzUserName);
					smsInteraction.setInteractionDateAndTime(now);
					smsInteraction.setInteractionDate(date);
					smsInteraction.setInteractionTime(time);
					smsInteraction.setResponseFromGateway(responseBody);
					smsInteraction.setSmsMessage(messageTemplate);
					//logger.info("to update status "+responseBody);
					
					if(responseBody.contains(sentSuccess)){
						smsInteraction.setSmsStatus(true);
					}else{
						
						smsInteraction.setSmsStatus(false);
					}
					
					int i=0;
					for(SMSstatus sa:statuscodes){
					if(responseBody.contains(sa.getCode())){
						
						smsInteraction.setReason(sa.getDescription());
						i++;
						
					}
					}
					if(i==0){
						
						smsInteraction.setReason("Not a valid status code");
					}
					

					em.merge(smsInteraction);

				//	return true;
				} else {

						return false;
				}

			} catch (URISyntaxException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();

					return false;
			}
			
			
			}
			return true;
		
		}

	

	@Override
	public List<CustomerSearchResult> getSelectedCustomerList(long savedsearchname , List<Long> selectedusers,boolean setFlag) {
		// TODO Auto-generated method stub
		return getFilteredSearchResult(savedsearchname,selectedusers, setFlag);
	}

	@Override
	public SMSTemplate getSmsTemplate(String smsType) {

		return source.smsTemplates(em).where(up -> up.getSmsType().equals(smsType)).getOnlyValue();

	}

	@Override
	public Phone getPhonenumberbyUploadId(long upId) {
		// TODO Auto-generated method stub
		//String uploadId=String.valueOf(upId);
		Phone prefphone = new Phone();
		List<Phone> phonelist= source.phones(em).where(up -> up.getCustomer().getId()==upId).toList();
		for(Phone p : phonelist){
			
			if(p.isPreferredPhone){
				prefphone = p;
				
			}
		}
		return prefphone;
	}

	@Override
	public SMSTemplate getSMSAPI(String userLogindealerCode) {
		// TODO Auto-generated method stub
		logger.info("userLogindealerCode:"+userLogindealerCode);
		return source.smsTemplates(em).where(s->s.getSmsType().equals("NONCONTACT")).getOnlyValue();
	}

	/*@Override
	public SMSTemplate getSMSAPIbyDealer(String dealer_Code) {
		logger.info("delaername:"+dealer_Code);
		// TODO Auto-generated method stub
		return source.smsTemplates(em).where(up -> up.getDealerName().equals(dealer_Code)).getOnlyValue();
	}*/
	
	
}