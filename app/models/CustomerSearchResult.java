package models;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class CustomerSearchResult {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public long id;
	
	@Column(length = 30)
	private String cid;
	@Column(length = 30)
	private String vehicle_id;
	@Column(length = 200)
	private String customerName;
	@Column(length = 50)
	private String vehicleRegNo;
	@Column(length = 50)
	private String customerCategory;
	@Column(length = 50)
	private String phoneNumber;
	@Column(length = 50)
	private String nextServicedate;
	@Column(length = 50)
	private String model;
	@Column(length = 50)
	private String variant;
	
	private long serviceId;
	
	@Column(length = 50)
	private String lastdisposition;

	
	public CustomerSearchResult(String cid, String vehicle_id, String customerName, String vehicleRegNo,
			String customerCategory, String phoneNumber, String nextServicedate, String model, String variant,
			long serviceId) {
		this.cid = cid;
		this.vehicle_id = vehicle_id;
		this.customerName = customerName;
		this.vehicleRegNo = vehicleRegNo;
		this.customerCategory = customerCategory;
		this.phoneNumber = phoneNumber;
		this.nextServicedate = nextServicedate;
		this.model = model;
		this.variant = variant;
		this.serviceId = serviceId;
	}

	public CustomerSearchResult() {
		this.cid = "";
		this.vehicle_id = "";
		this.customerName = "";
		this.vehicleRegNo = "";
		this.customerCategory = "";
		this.phoneNumber = "";
		this.nextServicedate = "";
		this.model = "";
		this.variant = "";
		this.serviceId = (long)0;

	}
	
	
	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getVehicleRegNo() {
		return vehicleRegNo;
	}

	public void setVehicleRegNo(String vehicleRegNo) {
		this.vehicleRegNo = vehicleRegNo;
	}

	public String getCustomerCategory() {
		return customerCategory;
	}

	public void setCustomerCategory(String customerCategory) {
		this.customerCategory = customerCategory;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getNextServicedate() {
		return nextServicedate;
	}

	public void setNextServicedate(String nextServicedate) {
		this.nextServicedate = nextServicedate;
	}

	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public String getVariant() {
		return variant;
	}

	public void setVariant(String variant) {
		this.variant = variant;
	}

	

	public long getServiceId() {
		return serviceId;
	}

	public void setServiceId(long serviceId) {
		this.serviceId = serviceId;
	}

	public String getCid() {
		return cid;
	}

	public void setCid(String cid) {
		this.cid = cid;
	}

	public String getVehicle_id() {
		return vehicle_id;
	}

	public void setVehicle_id(String vehicle_id) {
		this.vehicle_id = vehicle_id;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getLastdisposition() {
		return lastdisposition;
	}

	public void setLastdisposition(String lastdisposition) {
		this.lastdisposition = lastdisposition;
	}

	

}
