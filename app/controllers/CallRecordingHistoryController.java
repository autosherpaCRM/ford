package controllers;

import static play.libs.Json.toJson;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.inject.Inject;

import org.pac4j.core.profile.CommonProfile;
import org.pac4j.core.profile.ProfileManager;
import org.pac4j.play.PlayWebContext;
import org.pac4j.play.java.Secure;
import org.pac4j.play.store.PlaySessionStore;
import org.springframework.stereotype.Controller;

import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import controllers.webmodels.CallLogAjaxLoad;
import controllers.webmodels.CallRecordingDataTable;
import models.CallDispositionData;
import models.Campaign;
import models.WyzUser;
import play.Logger.ALogger;
import play.libs.Json;
import play.mvc.Result;
import repositories.CallInteractionsRepository;
import repositories.InsuranceRepository;
import repositories.WyzUserRepository;
import views.html.recording.*;

@Controller
public class CallRecordingHistoryController extends play.mvc.Controller {

	ALogger logger = play.Logger.of("application");
	private PlaySessionStore playSessionStore;
	private final WyzUserRepository repoWyzUser;
	private final CallInteractionsRepository call_int_repo;
	private final InsuranceRepository insuranceRepo;

	@Inject
	public CallRecordingHistoryController(PlaySessionStore playSessionStore, WyzUserRepository repositoryWyzUser,
			CallInteractionsRepository interRepo, InsuranceRepository insurRepo) {

		this.playSessionStore = playSessionStore;
		this.repoWyzUser = repositoryWyzUser;
		this.call_int_repo = interRepo;
		this.insuranceRepo = insurRepo;
	}

	private CommonProfile getUserProfile() {
		final PlayWebContext context = new PlayWebContext(ctx(), playSessionStore);
		final ProfileManager<CommonProfile> profileManager = new ProfileManager(context);
		logger.debug("............About to obtain the profiles..........");
		List<CommonProfile> profiles = profileManager.getAll(true);
		logger.debug("..............Obtained profiles........." + profiles);
		return profiles.get(0);
	}

	private String getUserLogindealerCode() {
		String dealer_Code = (String) getUserProfile().getAttribute("DEALER_CODE");

		return dealer_Code;

	}

	private String getUserLoginName() {
		String userloginname = getUserProfile().getId();

		return userloginname;

	}

	private String getDealerName() {
		String dealerName = (String) getUserProfile().getAttribute("DEALER_NAME");

		return dealerName;

	}

	private String getOEMOfDealer() {

		String oemIS = (String) getUserProfile().getAttribute("OEM");

		return oemIS;

	}

	@Secure(clients = "FormClient")
	public Result callRecordingView() {

		logger.info("recording page");

		List<CallDispositionData> dispoList = repoWyzUser.getDispositionListOfService();
		List<Campaign> campaignList = new ArrayList<Campaign>();
		List<Campaign> campaignListCampign = call_int_repo.getCampaignNamesBYType("Campaign");
		List<Campaign> campaignListremainder = call_int_repo.getCampaignNamesBYType("Service Reminder");
		campaignList.addAll(campaignListCampign);
		campaignList.addAll(campaignListremainder);

		List<WyzUser> crelist = repoWyzUser.getCREofCREManager(getUserLoginName(), getUserLogindealerCode());

		return ok(callRecordingPage.render(getUserLoginName(), getDealerName(), dispoList, campaignList, crelist));
	}

	@Secure(clients = "FormClient")
	public Result callRecordingData() {

		Map<String, String[]> paramMap = request().queryString();
		ObjectNode result = Json.newObject();

		String fromDateNew = "";
		String toDateNew = "";
		String callType = "";
		String campaignName = "";
		String creInitiated = "";
		String disposition = "";
		String usernameslist = "";

		if (paramMap.get("columns[0][search][value]") != null) {
			fromDateNew = paramMap.get("columns[0][search][value]")[0];
		}

		if (paramMap.get("columns[1][search][value]") != null) {
			toDateNew = paramMap.get("columns[1][search][value]")[0];
		}

		if (paramMap.get("columns[2][search][value]") != null) {
			callType = paramMap.get("columns[2][search][value]")[0];
			if (callType.equals("0")) {
				callType = "";
			}
		}
		if (paramMap.get("columns[3][search][value]") != null) {
			campaignName = paramMap.get("columns[3][search][value]")[0];
			if (campaignName.equals("0")) {
				campaignName = "";
			}
		}
		if (paramMap.get("columns[4][search][value]") != null) {
			creInitiated = paramMap.get("columns[4][search][value]")[0];
			if (creInitiated.equals("0")) {
				creInitiated = "";
			}
		}

		if (paramMap.get("columns[5][search][value]") != null) {
			disposition = paramMap.get("columns[5][search][value]")[0];
			if (disposition.equals("0")) {
				disposition = "";
			}
		}

		if (paramMap.get("columns[6][search][value]") != null) {
			usernameslist = paramMap.get("columns[6][search][value]")[0];
			if (usernameslist.equals("0")) {
				usernameslist = "";
			}
		}

		logger.info("fromDateNew : " + fromDateNew);
		logger.info("toDateNew : " + toDateNew);
		logger.info("callType : " + callType);
		logger.info("creInitiated : " + creInitiated);
		logger.info("disposition : " + disposition);
		logger.info("usernameslist : " + usernameslist);

		String searchPattern = "";
		boolean allflag = false;
		boolean globalSearch = false;

		if (paramMap.get("search[value]") != null) {
			searchPattern = paramMap.get("search[value]")[0];
		}

		if (searchPattern.length() == 0 && campaignName.length() == 0 && fromDateNew.length() == 0
				&& toDateNew.length() == 0 && callType.length() == 0 && creInitiated.length() == 0
				&& disposition.length() == 0 && usernameslist.length() == 0) {
			allflag = true;
			logger.info("search length is 0 ");
		}
		if (searchPattern.length() > 0) {
			globalSearch = true;
			logger.debug("Global Search is true");
		}
		logger.info("searchPattern :" + searchPattern);
		long fromIndex = Long.valueOf(paramMap.get("start")[0]);
		long toIndex = Long.valueOf(paramMap.get("length")[0]);

		long totalSize = insuranceRepo.getAllRecordingHistoryCount(getUserLoginName(), "", "", "", "", "", "", "", "",
				1);
		logger.info("totalSize of assigned interaction : " + totalSize);

		if (toIndex < 0) {
			toIndex = 10;

		}

		if (toIndex > totalSize) {

			toIndex = totalSize;
		}
		long patternCount = 0;
		int modelType = 1;

		List<CallRecordingDataTable> assignedList = insuranceRepo.getAllRecordingHistoryData(getUserLoginName(),
				fromDateNew, toDateNew, callType, campaignName, creInitiated, disposition, searchPattern, usernameslist,
				modelType, fromIndex, toIndex);

		if (!allflag) {
			if (globalSearch)
				// patternCount =
				// searchRepo.getAllAssignedInteraction(userdata.getId());
				patternCount = insuranceRepo.getAllRecordingHistoryCount(getUserLoginName(), fromDateNew, toDateNew,
						callType, campaignName, creInitiated, disposition,usernameslist, searchPattern, modelType);
			else
				patternCount = insuranceRepo.getAllRecordingHistoryCount(getUserLoginName(), fromDateNew, toDateNew,
						callType, campaignName, creInitiated, disposition, usernameslist,searchPattern, modelType);

		}
		logger.info("patternCount of customer : " + patternCount + " searchPattern : " + searchPattern + "fromIndex : "
				+ fromIndex + "toIndex : " + toIndex + "totalSize : " + totalSize);

		logger.info("allFlag : " + allflag + " global : " + globalSearch);

		result.put("draw", Integer.valueOf(paramMap.get("draw")[0]));
		result.put("recordsTotal", totalSize);

		if (allflag) {
			result.put("recordsFiltered", totalSize);
		} else {
			result.put("recordsFiltered", patternCount);
		}
		ArrayNode an = result.putArray("data");

		for (CallRecordingDataTable c : assignedList) {

			String downloadMediaMR = "<audio controls><source src='http://localhost:9000/CRE/audiostreamFile/"
					+ c.getCallinteraction_id() + "' type='audio/mp3'></audio>";

			ObjectNode row = Json.newObject();
			row.put("0", String.valueOf(c.getCallDate()));
			row.put("1", c.getCallTime());
			row.put("2", c.getCallType());
			row.put("3", c.getIsCallinitaited());
			row.put("4", c.getUserName());
			row.put("5", c.getCampaignName());
			row.put("6", c.getCustomerName());
			row.put("7", c.getPhoneNumber());
			row.put("8", c.getVehicleRegNo());
			row.put("9", c.getChassisNo());
			row.put("10", c.getDisposition());
			row.put("11", downloadMediaMR);
			
			an.add(row);
		}

		return ok(result);
	}

	@Secure(clients = "FormClient")
	public Result callRecordingViewInsurance() {

		logger.info("recording page");

		List<CallDispositionData> dispoList = repoWyzUser.getDispositionListOfInsurance();
		List<Campaign> campaignList = new ArrayList<Campaign>();
		List<Campaign> campaignListCampign = call_int_repo.getCampaignNamesBYType("Insurance");

		campaignList.addAll(campaignListCampign);

		List<WyzUser> crelist = repoWyzUser.getCREofCREManager(getUserLoginName(), getUserLogindealerCode());

		return ok(callRecordingPageInsurance.render(getUserLoginName(), getDealerName(), dispoList, campaignList,
				crelist));

	}

	@Secure(clients = "FormClient")
	public Result callRecordingDataInsurance() {

		Map<String, String[]> paramMap = request().queryString();
		ObjectNode result = Json.newObject();

		String fromDateNew = "";
		String toDateNew = "";
		String callType = "";
		String campaignName = "";
		String creInitiated = "";
		String disposition = "";
		String usernameslist = "";

		if (paramMap.get("columns[0][search][value]") != null) {
			fromDateNew = paramMap.get("columns[0][search][value]")[0];
		}

		if (paramMap.get("columns[1][search][value]") != null) {
			toDateNew = paramMap.get("columns[1][search][value]")[0];
		}

		if (paramMap.get("columns[2][search][value]") != null) {
			callType = paramMap.get("columns[2][search][value]")[0];
			if (callType.equals("0")) {
				callType = "";
			}
		}
		if (paramMap.get("columns[3][search][value]") != null) {
			campaignName = paramMap.get("columns[3][search][value]")[0];
			if (campaignName.equals("0")) {
				campaignName = "";
			}
		}
		if (paramMap.get("columns[4][search][value]") != null) {
			creInitiated = paramMap.get("columns[4][search][value]")[0];
			if (creInitiated.equals("0")) {
				creInitiated = "";
			}
		}

		if (paramMap.get("columns[5][search][value]") != null) {
			disposition = paramMap.get("columns[5][search][value]")[0];
			if (disposition.equals("0")) {
				disposition = "";
			}
		}

		if (paramMap.get("columns[6][search][value]") != null) {
			usernameslist = paramMap.get("columns[6][search][value]")[0];
			if (usernameslist.equals("0")) {
				usernameslist = "";
			}
		}

		logger.info("fromDateNew : " + fromDateNew);
		logger.info("toDateNew : " + toDateNew);
		logger.info("callType : " + callType);
		logger.info("creInitiated : " + creInitiated);
		logger.info("disposition : " + disposition);
		logger.info("usernameslist : " + usernameslist);

		String searchPattern = "";
		boolean allflag = false;
		boolean globalSearch = false;

		if (paramMap.get("search[value]") != null) {
			searchPattern = paramMap.get("search[value]")[0];
		}

		if (searchPattern.length() == 0 && campaignName.length() == 0 && fromDateNew.length() == 0
				&& toDateNew.length() == 0 && callType.length() == 0 && creInitiated.length() == 0
				&& disposition.length() == 0 && usernameslist.length() == 0) {
			allflag = true;
			logger.info("search length is 0 ");
		}
		if (searchPattern.length() > 0) {
			globalSearch = true;
			logger.debug("Global Search is true");
		}
		logger.info("searchPattern :" + searchPattern);
		long fromIndex = Long.valueOf(paramMap.get("start")[0]);
		long toIndex = Long.valueOf(paramMap.get("length")[0]);

		long totalSize = insuranceRepo.getAllRecordingHistoryCount(getUserLoginName(), "", "", "", "", "", "", "", "",
				3);
		logger.info("totalSize of assigned interaction : " + totalSize);

		if (toIndex < 0) {
			toIndex = 10;

		}

		if (toIndex > totalSize) {

			toIndex = totalSize;
		}
		long patternCount = 0;
		int modelType = 3;

		List<CallRecordingDataTable> assignedList = insuranceRepo.getAllRecordingHistoryData(getUserLoginName(),
				fromDateNew, toDateNew, callType, campaignName, creInitiated, disposition, searchPattern, usernameslist,
				modelType, fromIndex, toIndex);

		if (!allflag) {
			if (globalSearch)
				// patternCount =
				// searchRepo.getAllAssignedInteraction(userdata.getId());
				patternCount = insuranceRepo.getAllRecordingHistoryCount(getUserLoginName(), fromDateNew, toDateNew,
						callType, campaignName, creInitiated, disposition, usernameslist, searchPattern, modelType);

			else
				patternCount = insuranceRepo.getAllRecordingHistoryCount(getUserLoginName(), fromDateNew, toDateNew,
						callType, campaignName, creInitiated, disposition, usernameslist, searchPattern, modelType);

		}
		logger.info("patternCount of customer : " + patternCount + " searchPattern : " + searchPattern + "fromIndex : "
				+ fromIndex + "toIndex : " + toIndex + "totalSize : " + totalSize);

		logger.info("allFlag : " + allflag + " global : " + globalSearch);

		result.put("draw", Integer.valueOf(paramMap.get("draw")[0]));
		result.put("recordsTotal", totalSize);

		if (allflag) {
			result.put("recordsFiltered", totalSize);
		} else {
			result.put("recordsFiltered", patternCount);
		}
		ArrayNode an = result.putArray("data");

		for (CallRecordingDataTable c : assignedList) {

			String downloadMediaMR = "<audio controls><source src='http://localhost:9000/CRE/audiostreamFile/"
					+ c.getCallinteraction_id() + "' type='audio/mp3'></audio>";

			ObjectNode row = Json.newObject();
			row.put("0", String.valueOf(c.getCallDate()));
			row.put("1", c.getCallTime());
			row.put("2", c.getCallType());
			row.put("3", c.getIsCallinitaited());
			row.put("4", c.getUserName());
			row.put("5", c.getCampaignName());
			row.put("6", c.getCustomerName());
			row.put("7", c.getPhoneNumber());
			row.put("8", c.getVehicleRegNo());
			row.put("9", c.getChassisNo());
			row.put("10", c.getDisposition());
			row.put("11", downloadMediaMR);
			an.add(row);
		}

		return ok(result);

	}

	@Secure(clients = "FormClient")
	public Result callRecordingViewPSF() {

		logger.info("recording page");

		List<CallDispositionData> dispoList = repoWyzUser.getDispositionListOfPSF();
		List<Campaign> campaignList = new ArrayList<Campaign>();
		List<Campaign> campaignListCampign = call_int_repo.getCampaignNamesBYType("PSF");

		campaignList.addAll(campaignListCampign);

		List<WyzUser> crelist = repoWyzUser.getCREofCREManager(getUserLoginName(), getUserLogindealerCode());

		return ok(callRecordingPagePSF.render(getUserLoginName(), getDealerName(), dispoList, campaignList, crelist));

	}

	@Secure(clients = "FormClient")
	public Result callRecordingDataPSF() {

		Map<String, String[]> paramMap = request().queryString();
		ObjectNode result = Json.newObject();

		String fromDateNew = "";
		String toDateNew = "";
		String callType = "";
		String campaignName = "";
		String creInitiated = "";
		String disposition = "";
		String usernameslist = "";

		if (paramMap.get("columns[0][search][value]") != null) {
			fromDateNew = paramMap.get("columns[0][search][value]")[0];
		}

		if (paramMap.get("columns[1][search][value]") != null) {
			toDateNew = paramMap.get("columns[1][search][value]")[0];
		}

		if (paramMap.get("columns[2][search][value]") != null) {
			callType = paramMap.get("columns[2][search][value]")[0];
			if (callType.equals("0")) {
				callType = "";
			}
		}
		if (paramMap.get("columns[3][search][value]") != null) {
			campaignName = paramMap.get("columns[3][search][value]")[0];
			if (campaignName.equals("0")) {
				campaignName = "";
			}
		}
		if (paramMap.get("columns[4][search][value]") != null) {
			creInitiated = paramMap.get("columns[4][search][value]")[0];
			if (creInitiated.equals("0")) {
				creInitiated = "";
			}
		}

		if (paramMap.get("columns[5][search][value]") != null) {
			disposition = paramMap.get("columns[5][search][value]")[0];
			if (disposition.equals("0")) {
				disposition = "";
			}
		}

		if (paramMap.get("columns[6][search][value]") != null) {
			usernameslist = paramMap.get("columns[6][search][value]")[0];
			if (usernameslist.equals("0")) {
				usernameslist = "";
			}
		}

		logger.info("fromDateNew : " + fromDateNew);
		logger.info("toDateNew : " + toDateNew);
		logger.info("callType : " + callType);
		logger.info("creInitiated : " + creInitiated);
		logger.info("disposition : " + disposition);
		logger.info("usernameslist : " + usernameslist);

		String searchPattern = "";
		boolean allflag = false;
		boolean globalSearch = false;

		if (paramMap.get("search[value]") != null) {
			searchPattern = paramMap.get("search[value]")[0];
		}

		if (searchPattern.length() == 0 && campaignName.length() == 0 && fromDateNew.length() == 0
				&& toDateNew.length() == 0 && callType.length() == 0 && creInitiated.length() == 0
				&& disposition.length() == 0 && usernameslist.length() == 0) {
			allflag = true;
			logger.info("search length is 0 ");
		}
		if (searchPattern.length() > 0) {
			globalSearch = true;
			logger.debug("Global Search is true");
		}
		logger.info("searchPattern :" + searchPattern);
		long fromIndex = Long.valueOf(paramMap.get("start")[0]);
		long toIndex = Long.valueOf(paramMap.get("length")[0]);

		long totalSize = insuranceRepo.getAllRecordingHistoryCount(getUserLoginName(), "", "", "", "", "", "", "", "",
				2);
		logger.info("totalSize of assigned interaction : " + totalSize);

		if (toIndex < 0) {
			toIndex = 10;

		}

		if (toIndex > totalSize) {

			toIndex = totalSize;
		}
		long patternCount = 0;
		int modelType = 2;

		List<CallRecordingDataTable> assignedList = insuranceRepo.getAllRecordingHistoryData(getUserLoginName(),
				fromDateNew, toDateNew, callType, campaignName, creInitiated, disposition, searchPattern, usernameslist,
				modelType, fromIndex, toIndex);

		if (!allflag) {
			if (globalSearch)
				// patternCount =
				// searchRepo.getAllAssignedInteraction(userdata.getId());
				patternCount = insuranceRepo.getAllRecordingHistoryCount(getUserLoginName(), fromDateNew, toDateNew,
						callType, campaignName, creInitiated, disposition, usernameslist,searchPattern, modelType);
			else
				patternCount = insuranceRepo.getAllRecordingHistoryCount(getUserLoginName(), fromDateNew, toDateNew,
						callType, campaignName, creInitiated, disposition, usernameslist,searchPattern, modelType);

		}
		logger.info("patternCount of customer : " + patternCount + " searchPattern : " + searchPattern + "fromIndex : "
				+ fromIndex + "toIndex : " + toIndex + "totalSize : " + totalSize);

		logger.info("allFlag : " + allflag + " global : " + globalSearch);

		result.put("draw", Integer.valueOf(paramMap.get("draw")[0]));
		result.put("recordsTotal", totalSize);

		if (allflag) {
			result.put("recordsFiltered", totalSize);
		} else {
			result.put("recordsFiltered", patternCount);
		}
		ArrayNode an = result.putArray("data");

		for (CallRecordingDataTable c : assignedList) {

			String downloadMediaMR = "<audio controls><source src='http://localhost:9000/CRE/audiostreamFile/"
					+ c.getCallinteraction_id() + "' type='audio/mp3'></audio>";

			ObjectNode row = Json.newObject();
			row.put("0", String.valueOf(c.getCallDate()));
			row.put("1", c.getCallTime());
			row.put("2", c.getCallType());
			row.put("3", c.getIsCallinitaited());
			row.put("4", c.getUserName());
			row.put("5", c.getCampaignName());
			row.put("6", c.getCustomerName());
			row.put("7", c.getPhoneNumber());
			row.put("8", c.getVehicleRegNo());
			row.put("9", c.getChassisNo());
			row.put("10", c.getDisposition());
			row.put("11", downloadMediaMR);
			an.add(row);
		}

		return ok(result);

	}
	
	
	@Secure(clients = "FormClient")
	public Result otherCallRecordingView() {

		logger.info("other recording page");

		List<WyzUser> crelist = repoWyzUser.getCREofCREManager(getUserLoginName(), getUserLogindealerCode());

		return ok(otherCallRecordingPage.render(getUserLoginName(), getDealerName(),crelist));

	}
	@Secure(clients = "FormClient")
	public Result otherCallRecordingData(){
		
		Map<String, String[]> paramMap = request().queryString();
		ObjectNode result = Json.newObject();

		String fromDateNew = "";
		String toDateNew = "";
		String callType = "";		
		String usernameslist = "";

		if (paramMap.get("columns[0][search][value]") != null) {
			fromDateNew = paramMap.get("columns[0][search][value]")[0];
		}

		if (paramMap.get("columns[1][search][value]") != null) {
			toDateNew = paramMap.get("columns[1][search][value]")[0];
		}

		if (paramMap.get("columns[2][search][value]") != null) {
			callType = paramMap.get("columns[2][search][value]")[0];
			if (callType.equals("0")) {
				callType = "";
			}
		}	

		if (paramMap.get("columns[6][search][value]") != null) {
			usernameslist = paramMap.get("columns[6][search][value]")[0];
			if (usernameslist.equals("0")) {
				usernameslist = "";
			}
		}

		logger.info("fromDateNew : " + fromDateNew);
		logger.info("toDateNew : " + toDateNew);
		logger.info("callType : " + callType);		
		logger.info("usernameslist : " + usernameslist);

		String searchPattern = "";
		boolean allflag = false;
		boolean globalSearch = false;

		if (paramMap.get("search[value]") != null) {
			searchPattern = paramMap.get("search[value]")[0];
		}

		if (searchPattern.length() == 0  && fromDateNew.length() == 0
				&& toDateNew.length() == 0 && callType.length() == 0 
				&& usernameslist.length() == 0) {
			allflag = true;
			logger.info("search length is 0 ");
		}
		if (searchPattern.length() > 0) {
			globalSearch = true;
			logger.debug("Global Search is true");
		}
		logger.info("searchPattern :" + searchPattern);
		long fromIndex = Long.valueOf(paramMap.get("start")[0]);
		long toIndex = Long.valueOf(paramMap.get("length")[0]);

		long totalSize = insuranceRepo.getOtherAllRecordingHistoryCount(getUserLoginName(),"", "", "","", "");
		logger.info("totalSize of assigned interaction : " + totalSize);

		if (toIndex < 0) {
			toIndex = 10;

		}

		if (toIndex > totalSize) {

			toIndex = totalSize;
		}
		long patternCount = 0;
		int modelType = 2;

		List<CallRecordingDataTable> assignedList = insuranceRepo.getOtherAllRecordingHistoryData(getUserLoginName(),
				fromDateNew, toDateNew, usernameslist,callType, searchPattern,fromIndex, toIndex);

		if (!allflag) {
			if (globalSearch){
				
				patternCount = insuranceRepo.getOtherAllRecordingHistoryCount(getUserLoginName(),
						fromDateNew, toDateNew, usernameslist,callType, searchPattern);
			
			logger.info("globalSearch" +patternCount);}
			else{
				patternCount = insuranceRepo.getOtherAllRecordingHistoryCount(getUserLoginName(),
						fromDateNew, toDateNew, usernameslist,callType, searchPattern);
				logger.info("getUserLoginName()" +getUserLoginName()+ " fromDateNew : "+fromDateNew+" toDateNew : "+toDateNew+
						" usernameslist : "+usernameslist+" callType : "+callType+" searchPattern : "+searchPattern );
				
				
				logger.info("globalSearch false" +patternCount);
			}

		}
		logger.info("patternCount of customer : " + patternCount + " searchPattern : " + searchPattern + "fromIndex : "
				+ fromIndex + "toIndex : " + toIndex + "totalSize : " + totalSize);

		logger.info("allFlag : " + allflag + " global : " + globalSearch);

		result.put("draw", Integer.valueOf(paramMap.get("draw")[0]));
		result.put("recordsTotal", totalSize);

		if (allflag) {
			result.put("recordsFiltered", totalSize);
		} else {
			result.put("recordsFiltered", patternCount);
		}
		ArrayNode an = result.putArray("data");

		for (CallRecordingDataTable c : assignedList) {

			String downloadMediaMR = "<audio controls><source src='http://localhost:9000/CRE/audiostreamFile/"
					+ c.getCallinteraction_id() + "' type='audio/mp3'></audio>";

			ObjectNode row = Json.newObject();
			row.put("0", String.valueOf(c.getCallDate()));
			row.put("1", c.getCallTime());
			row.put("2", c.getCallType());
			row.put("3", c.getUserName());
			row.put("4", c.getCustomerName());
			row.put("5", c.getPhoneNumber());
			row.put("6", c.getVehicleRegNo());
			row.put("7", c.getChassisNo());
			row.put("8", downloadMediaMR);
			an.add(row);
		}

		return ok(result);
	}
}
