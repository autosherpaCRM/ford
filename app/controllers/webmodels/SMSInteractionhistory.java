package controllers.webmodels;

public class SMSInteractionhistory {

	public String interactionDate;
	public String interactionTime;
	public String smsMessage;
	public boolean smsStatus;
	public String CustomerName;
	public String VehicleRegNo;
	public String WyzuserName;
	public String mobileNumber;
	public String reason;

	public SMSInteractionhistory() {
		this.interactionDate = "";
		this.interactionTime = "";
		this.smsMessage = "";
		this.smsStatus = false;
		this.CustomerName = "";
		this.VehicleRegNo = "";
		this.WyzuserName = "";
		this.mobileNumber = "";
		this.reason = "";
	}

	public SMSInteractionhistory(String interactionDate, String interactionTime, String smsMessage, boolean smsStatus,
			String customerName, String vehicleRegNo, String wyzuserName, String mobileNumber,
			String reason) {
		this.interactionDate = interactionDate;
		this.interactionTime = interactionTime;
		this.smsMessage = smsMessage;
		this.smsStatus = smsStatus;
		this.CustomerName = customerName;
		this.VehicleRegNo = vehicleRegNo;
		this.WyzuserName = wyzuserName;
		this.mobileNumber = mobileNumber;
		this.reason = reason;
	}

	public String getInteractionDate() {
		return interactionDate;
	}

	public void setInteractionDate(String interactionDate) {
		this.interactionDate = interactionDate;
	}

	public String getInteractionTime() {
		return interactionTime;
	}

	public void setInteractionTime(String interactionTime) {
		this.interactionTime = interactionTime;
	}

	public String getSmsMessage() {
		return smsMessage;
	}

	public void setSmsMessage(String smsMessage) {
		this.smsMessage = smsMessage;
	}

	public String getCustomerName() {
		return CustomerName;
	}

	public void setCustomerName(String customerName) {
		CustomerName = customerName;
	}

	public String getVehicleRegNo() {
		return VehicleRegNo;
	}

	public void setVehicleRegNo(String vehicleRegNo) {
		VehicleRegNo = vehicleRegNo;
	}

	public String getWyzuserName() {
		return WyzuserName;
	}

	public void setWyzuserName(String wyzuserName) {
		WyzuserName = wyzuserName;
	}

	public boolean isSmsStatus() {
		return smsStatus;
	}

	public void setSmsStatus(boolean smsStatus) {
		this.smsStatus = smsStatus;
	}

	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

}
