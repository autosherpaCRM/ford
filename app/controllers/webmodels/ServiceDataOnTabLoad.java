/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers.webmodels;

import java.util.Date;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author W-885
 */
public class ServiceDataOnTabLoad {
    
    public long id;
    
    public String jobCardDate;
    public String jobCardNumber;
    public String serviceAdvisor;
    public String psfstatus;
    @Temporal(TemporalType.DATE)
    public Date   serviceDate;    
    public String serviceType;        
    public String lastServiceMeterReading;
    public String serviceLocaton;
    public String kilometer;
    public String category;
    public double partAmt;
    public double labourAmt;
    public double totalAmt;
    public double mfgPartsTotal;
	public double localPartsTotal;
	public double maximileTotal;
	public double oilConsumablesTotal;
	public double maxiCareTotal;
	public double mfgAccessoriesTotal;
	public double localAccessoriesTotal;
	public double inhouseLabourTotal;
	public double outLabourTotal;
	public String menuCodeDesc;
	public double billAmt;
	
	
	public double getBillAmt() {
		return billAmt;
	}

	public void setBillAmt(double billAmt) {
		this.billAmt = billAmt;
	}

	public String getMenuCodeDesc() {
		return menuCodeDesc;
	}

	public void setMenuCodeDesc(String menuCodeDesc) {
		this.menuCodeDesc = menuCodeDesc;
	}

	public double getMfgPartsTotal() {
		return mfgPartsTotal;
	}

	public void setMfgPartsTotal(double mfgPartsTotal) {
		this.mfgPartsTotal = mfgPartsTotal;
	}

	public double getLocalPartsTotal() {
		return localPartsTotal;
	}

	public void setLocalPartsTotal(double localPartsTotal) {
		this.localPartsTotal = localPartsTotal;
	}

	public double getMaximileTotal() {
		return maximileTotal;
	}

	public void setMaximileTotal(double maximileTotal) {
		this.maximileTotal = maximileTotal;
	}

	public double getOilConsumablesTotal() {
		return oilConsumablesTotal;
	}

	public void setOilConsumablesTotal(double oilConsumablesTotal) {
		this.oilConsumablesTotal = oilConsumablesTotal;
	}

	public double getMaxiCareTotal() {
		return maxiCareTotal;
	}

	public void setMaxiCareTotal(double maxiCareTotal) {
		this.maxiCareTotal = maxiCareTotal;
	}

	public double getMfgAccessoriesTotal() {
		return mfgAccessoriesTotal;
	}

	public void setMfgAccessoriesTotal(double mfgAccessoriesTotal) {
		this.mfgAccessoriesTotal = mfgAccessoriesTotal;
	}

	public double getLocalAccessoriesTotal() {
		return localAccessoriesTotal;
	}

	public void setLocalAccessoriesTotal(double localAccessoriesTotal) {
		this.localAccessoriesTotal = localAccessoriesTotal;
	}

	public double getInhouseLabourTotal() {
		return inhouseLabourTotal;
	}

	public void setInhouseLabourTotal(double inhouseLabourTotal) {
		this.inhouseLabourTotal = inhouseLabourTotal;
	}

	public double getOutLabourTotal() {
		return outLabourTotal;
	}

	public void setOutLabourTotal(double outLabourTotal) {
		this.outLabourTotal = outLabourTotal;
	}

	public double getPartAmt() {
		return partAmt;
	}

	public void setPartAmt(double partAmt) {
		this.partAmt = partAmt;
	}

	public double getLabourAmt() {
		return labourAmt;
	}

	public void setLabourAmt(double labourAmt) {
		this.labourAmt = labourAmt;
	}

	public double getTotalAmt() {
		return totalAmt;
	}

	public void setTotalAmt(double totalAmt) {
		this.totalAmt = totalAmt;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getKilometer() {
		return kilometer;
	}

	public void setKilometer(String kilometer) {
		this.kilometer = kilometer;
	}

	public String custName;
    
    public String getCustName() {
		return custName;
	}

	public void setCustName(String custName) {
		this.custName = custName;
	}

	@Temporal(TemporalType.DATE)
    public Date billDate;
    
    public Date getBillDate() {
		return billDate;
	}

	public void setBillDate(Date billDate) {
		this.billDate = billDate;
	}

	@Temporal(TemporalType.DATE)
    public Date nextServicedate;
    
    @Temporal(TemporalType.DATE)
    public Date saleDate;

    

	public String getJobCardDate() {
		return jobCardDate;
	}

	public void setJobCardDate(String jobCardDate) {
		this.jobCardDate = jobCardDate;
	}

	public String getServiceLocaton() {
        return serviceLocaton;
    }

    public void setServiceLocaton(String serviceLocaton) {
        this.serviceLocaton = serviceLocaton;
    }
    
    
    public Date getServiceDate() {
        return serviceDate;
    }

    public void setServiceDate(Date serviceDate) {
        this.serviceDate = serviceDate;
    }

    public String getServiceType() {
        return serviceType;
    }

    public void setServiceType(String serviceType) {
        this.serviceType = serviceType;
    }

    public String getLastServiceMeterReading() {
        return lastServiceMeterReading;
    }

    public void setLastServiceMeterReading(String lastServiceMeterReading) {
        this.lastServiceMeterReading = lastServiceMeterReading;
    }
    
    
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getServiceAdvisor() {
        return serviceAdvisor;
    }

    public void setServiceAdvisor(String serviceAdvisor) {
        this.serviceAdvisor = serviceAdvisor;
    }

    public String getPsfstatus() {
        return psfstatus;
    }

    public void setPsfstatus(String psfstatus) {
        this.psfstatus = psfstatus;
    }
    
    

    public String getJobCardNumber() {
        return jobCardNumber;
    }

    public void setJobCardNumber(String jobCardNumber) {
        this.jobCardNumber = jobCardNumber;
    }
    
    
    public Date getNextServicedate() {
        return nextServicedate;
    }

    public void setNextServicedate(Date nextServicedate) {
        this.nextServicedate = nextServicedate;
    }

    public Date getSaleDate() {
        return saleDate;
    }

    public void setSaleDate(Date saleDate) {
        this.saleDate = saleDate;
    }

    
}
