package controllers.webmodels;

import java.util.Date;

public class ReportSummary {
	
	public Date Date;
	public String day;
	public String Reportdate;
	public String count;
	public String WorkshopOrCity;
	
	
	
	public ReportSummary(Date Date, String day, String reportdate, String count, String workshopOrCity) {
		super();
		this.Date = Date;
		this.day = day;
		this.Reportdate = reportdate;
		this.count = count;
		this.WorkshopOrCity = workshopOrCity;
	}
	
	
	public ReportSummary() {
		
		this.Date = null;
		this.day = "";
		this.Reportdate = "";
		this.count = "";
		this.WorkshopOrCity = "";
	}
	
	public Date getDate() {
		return Date;
	}
	public void setDate(Date Date) {
		this.Date = Date;
	}
	public String getDay() {
		return day;
	}
	public void setDay(String day) {
		this.day = day;
	}
	public String getReportdate() {
		return Reportdate;
	}
	public void setReportdate(String reportdate) {
		Reportdate = reportdate;
	}
	public String getCount() {
		return count;
	}
	public void setCount(String count) {
		this.count = count;
	}
	public String getWorkshopOrCity() {
		return WorkshopOrCity;
	}
	public void setWorkshopOrCity(String workshopOrCity) {
		WorkshopOrCity = workshopOrCity;
	}
	
	
	

}
